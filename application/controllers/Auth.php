<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        // memberikan rules
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        // jika form tidak tervalidasi
        if ($this->form_validation->run() == false) {
            // tampilkan halaman login
            $data['title'] = 'PressPlay | Login';
            $this->template_auth->load('auth/login', $data);
        }
        // jika form tervalidasi
        else {
            // melakukan login
            $this->_login();
        }
    }

    private function _login()
    {
        // mengambil input
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // membuat query
        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        // check apakah user ada --> !null
        if ($user) {
            // check apakah user aktif (apabila ada aktivasi akun lewat email)
            if ($user['is_active']) {
                // check apakah input password sesuai dengan password di database
                if (password_verify($password, $user['password'])) {
                    // membuat data untuk session
                    $data = [
                        'id' => $user['id'],
                        'first_name' => $user['first_name'],
                        'email' => $user['email'],
                        'role_id' => $user['role_id'],
                    ];

                    // set_userdata untuk session
                    $this->session->set_userdata($data);
                    $this->session->set_flashdata('login_ok', "<div class='alert alert-login'>
                    Selamat datang kembali, {$user['first_name']}! </div>");

                    // check user role
                    if ($user['role_id'] == 2) {
                        if (isset($_SESSION['current_product'])) {
                            $id = $_SESSION['current_product']['product_id'];
                            redirect("Shop/product/$id");
                        } else {
                            redirect('Landing');
                        }
                    } else {
                        redirect('administrator/Dashboard');
                    }
                    // redirect ke controller user/index
                }
                // password salah
                else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                        Wrong password!
                    </div>');
                    redirect('auth');
                }
            }
            // email belum di aktivasi 
            else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    This email has not been activated!
                </div>');
                redirect('auth');
            }
        }
        // tidak ditemukan user dengan email yang sesuai
        else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Wrong email or password!
          </div>');
            redirect('auth');
        }
    }

    public function registration()
    {
        // melakukan validasi form
        $this->form_validation->set_rules('first_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'Email already registered!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
            'matches' => 'password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        // jika validasi gagal
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Page | Registration';
            $this->template_auth->load('auth/register', $data);
        }
        // jika validasi berhasil
        else {
            // membuat data array untuk query ke db (harus urut sesuai db)
            $data = [
                'first_name' => htmlspecialchars($this->input->post('first_name', true)),
                'last_name' => htmlspecialchars($this->input->post('last_name', true)),
                'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'address' => htmlspecialchars($this->input->post('address', true)),
                'role_id' => 2,
                'is_active' => 1
                // 'registered_at' => time(),
            ];
            // menginputkan data registrasi
            $this->db->insert('user', $data);

            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Welcome, new user! Lets login!</div>');

            // redirect ke controller auth, atau login
            redirect('auth');
        }
    }

    public function logout()
    {
        // unset userdata yang di set ketika login
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('first_name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('current_product');

        // membuat flashdata message untuk menandakan berhasil logout
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kamu berhasil Logout
          </div>');

        // redirect ke halaman login
        redirect('auth');
    }
}
