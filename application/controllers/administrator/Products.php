<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('product_model');
        if (isset($this->session->userdata['role_id'])) {
            if ($this->session->userdata['role_id'] == 2) {
                redirect('auth/logout');
                // membuat flashdata message untuk menandakan perlu login
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Kamu perlu login terlebih dahulu
                </div>');
                redirect('auth');
            }
        } else {
            // membuat flashdata message untuk menandakan perlu login
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Kamu perlu login terlebih dahulu
            </div>');
            redirect('auth');
        }
    }

    public function display_products($search = null)
    {
        if ($search) {
            return $this->product_model->get_all_product($search);
        }
        if ($this->session->userdata('role_id') == 1) {

            return $this->product_model->get_all_product();
        }
        return $this->product_model->product_by_status();
    }

    public function index()
    {
        if ($this->input->get()) {
            $get_data = $this->input->get();
            if (isset($get_data['kategori'])) {
                if ($get_data['kategori'] == null) {
                    $this->db->select('*');
                    $this->db->from('products');
                    $this->db->like('nama', $get_data['search']);
                } else {
                    $this->db->select('*');
                    $this->db->from('products');
                    $this->db->where('category', $get_data['kategori']);
                    $this->db->like('nama', $get_data['search']);
                }
                $data['current_category'] = $get_data['kategori'];
                $result = $this->db->get()->result_array();
                $data['search'] = $get_data['search'];
                $data['products'] = $result;
            } else {
                $this->db->select('*');
                $this->db->from('products');
                $this->db->like('nama', $get_data['search']);
                $result = $this->db->get()->result_array();
                $data['current_category'] = 'Semua Kategori';
                $data['search'] = $get_data['search'];
                $data['products'] = $result;
            }
        } else {
            $data['products'] = $this->display_products();
            $data['current_category'] = 'Semua Kategori';
            $data['search'] = '';

            // print_r($data);
        }
        $data['title'] = "Product";
        $data['categories'] = $this->product_model->get_category();
        $this->template_admin->load('admin/products/products', $data);
    }

    public function online_product()
    {
        if ($this->input->get()) {
            $get_data = $this->input->get();
            if ($get_data['kategori'] == null) {
                $this->db->like('nama', $get_data['search']);
                $this->db->from('products');
                $this->db->join('sale', 'sale.product_id = products.id');
                $this->db->order_by('count', 'DESC');
            } else {
                $this->db->where('category', $get_data['kategori']);
                $this->db->like('nama', $get_data['search']);
                $this->db->from('products');
                $this->db->join('sale', 'sale.product_id = products.id');
                $this->db->order_by('count', 'DESC');
            }
            $data['current_category'] = $get_data['kategori'];
            $data['search'] = $get_data['search'];
            $result = $this->db->get()->result_array();
            $data['products'] = $result;
        } else {
            $data['products'] = $this->product_model->product_by_sale();
            $data['current_category'] = 'Semua Kategori';
            $data['search'] = '';

            // print_r($data);
        }
        $data['title'] = "Press Play | Product";
        $data['categories'] = $this->product_model->get_category();
        $this->template_admin->load('admin/products/online_products', $data);
    }

    public function add_product()
    {
        // melakukan validasi form
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('stok', 'Stock', 'required|trim');
        $this->form_validation->set_rules('detail', 'Details', 'required|trim');
        $this->form_validation->set_rules('kategori', 'Category', 'required|trim');
        $this->form_validation->set_rules('harga', 'Price', 'required|trim');

        // jika validasi gagal
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Page | tambah produk';
            $this->template_admin->load('admin/products/add_product', $data);
        }

        // jika validasi berhasil
        else {

            $image = $_FILES['image'];

            if ($_FILES['image'] != '') {
                $config['upload_path'] = './assets/img';
                $config['allowed_types'] = 'jpg|png|gif';

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    echo 'upload gagal';
                    die();
                } else {
                    $image = $this->upload->data('file_name');
                }
            }
            // membuat data array untuk query ke db (harus urut sesuai db)
            $data = [
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'details' => htmlspecialchars($this->input->post('detail', true)),
                'stock' => htmlspecialchars($this->input->post('stok', true)),
                'price' => htmlspecialchars($this->input->post('harga', true)),
                'image' => $image,
                'category' => htmlspecialchars($this->input->post('kategori', true)),
                'status' => 'PENDING',
                'created_by' => $_SESSION['id'],
                // 'registered_at' => time(),
            ];
            // menginputkan data registrasi
            $this->db->insert('products', $data);

            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil memasukkan data</div>');

            // redirect ke controller auth, atau login
            redirect('administrator/products');
        }
    }

    public function update_product($id)
    {
        // melakukan validasi form
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('stok', 'Stock', 'required|trim');
        $this->form_validation->set_rules('detail', 'Details', 'required|trim');
        $this->form_validation->set_rules('kategori', 'Category', 'required|trim');
        $this->form_validation->set_rules('harga', 'Price', 'required|trim');

        // jika validasi gagal
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Page | Update produk';
            $produk = $this->product_model->get_product($id);
            $data['produk'] = $produk;
            $data['id'] = $id;
            // print_r($data);
            $this->template_admin->load('admin/products/update_product', $data);
        }

        // jika validasi berhasil
        else {

            $data = [
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'details' => htmlspecialchars($this->input->post('detail', true)),
                'stock' => htmlspecialchars($this->input->post('stok', true)),
                'price' => htmlspecialchars($this->input->post('harga', true)),
                'category' => htmlspecialchars($this->input->post('kategori', true)),
                'status' => 'PENDING',
                'created_by' => $_SESSION['id'],
                // 'registered_at' => time(),
            ];
            // menginputkan data registrasi

            $this->db->where('id', $id);
            $this->db->update('products', $data);

            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil mengupdate data</div>');

            // redirect ke controller products
            redirect('administrator/products');
        }
    }

    public function approve_product($id)
    {
        $this->db->where('id', $id);
        $this->db->update('products', array('status' => 'APPROVED'));

        $this->db->select('*');
        $this->db->from('sale');
        $this->db->where('product_id', $id);
        $get = $this->db->get()->num_rows();
        // sale
        $data = [
            'product_id' => $id,
            'count' => 0,
            'total' => 0
        ];

        if (!$get) {
            $this->db->insert('sale', $data);
        }
        redirect('administrator/products');
    }

    public function unapprove_product($id)
    {
        $this->db->where('id', $id);
        $this->db->update('products', array('status' => 'REJECTED'));
        redirect('administrator/products');
    }

    public function delete_product($id, $img)
    {
        $file_path = "assets/img/$img";
        if (unlink($file_path)) {
            $this->db->delete('products', array('id' => $id));
            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil menghapus produk</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">
            gagal menghapus produk</div>');
        }
        redirect('administrator/products');
    }
}
