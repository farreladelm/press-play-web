<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hero_poster extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('hero_model');
        if (isset($this->session->userdata['role_id'])) {
            if ($this->session->userdata['role_id'] == 2) {
                redirect('auth/logout');
                // membuat flashdata message untuk menandakan perlu login
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Kamu perlu login terlebih dahulu
                </div>');
                redirect('auth');
            }
        } else {
            // membuat flashdata message untuk menandakan perlu login
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Kamu perlu login terlebih dahulu
            </div>');
            redirect('auth');
        }
    }

    public function index()
    {
        $data['heros'] = $this->hero_model->get_all_hero();
        $this->template_admin->load('admin/hero/hero_poster', $data);
    }

    public function add_hero()
    {
        // melakukan validasi form
        $this->form_validation->set_rules('label', 'Label', 'required|trim');
        $this->form_validation->set_rules('priority', 'Priority', 'required|trim');

        // jika validasi gagal
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Page | tambah produk';
            $this->template_admin->load('admin/hero/add_hero', $data);
        }

        // jika validasi berhasil
        else {

            $image = $_FILES['image'];

            if ($_FILES['image'] != '') {
                $config['upload_path'] = './assets/img';
                $config['allowed_types'] = 'jpg|png|gif';

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    // memberikan flashdata message kalau registrasi berhasil
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">
                    Gagal Upload file gambar</div>');
                    redirect('administrator/hero_poster/add_hero');
                    die();
                } else {
                    $image = $this->upload->data('file_name');
                    $this->session->set_flashdata('message', '<div class="alert alert-success">
                    Upload berhasil</div>');
                }
            }
            // membuat data array untuk query ke db (harus urut sesuai db)
            $data = [
                'label' => htmlspecialchars($this->input->post('label', true)),
                'alamat_img' => $image,
                'priority' => htmlspecialchars($this->input->post('priority', true)),
                'status' => 'PENDING',
                // 'registered_at' => time(),
            ];

            // menginputkan data registrasi
            $this->db->insert('hero_img', $data);

            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil memasukkan data</div>');

            // redirect ke controller auth, atau login
            redirect('administrator/hero_poster');
        }
    }
    public function update_hero($id)
    {
        // melakukan validasi form
        $this->form_validation->set_rules('label', 'Label', 'required|trim');
        $this->form_validation->set_rules('priority', 'Priority', 'required|trim');

        // jika validasi gagal
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Pressplay | Update Hero';
            $data['hero'] = $this->hero_model->get_hero($id);
            $data['id'] = $id;
            // print_r($data);
            $this->template_admin->load('admin/hero/update_hero', $data);
        }

        // jika validasi berhasil
        else {
            $data = [
                'label' => htmlspecialchars($this->input->post('label', true)),
                'priority' => htmlspecialchars($this->input->post('priority', true)),
            ];

            // menginputkan data registrasi
            $this->db->where('id', $id);
            $this->db->update('hero_img', $data);

            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil update hero</div>');


            // redirect ke controller auth, atau login
            redirect('administrator/hero_poster');
        }
    }


    public function approve_hero($id)
    {
        if ($this->hero_model->approve_hero($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil Approve hero</div>');
            redirect('administrator/hero_poster');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">
            Gagal approve hero</div>');
            redirect('administrator/hero_poster');
        }
    }
    public function unapprove_hero($id)
    {
        if ($this->hero_model->unapprove_hero($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil Unapprove hero</div>');
            redirect('administrator/hero_poster');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">
            Gagal unapprove hero</div>');
            redirect('administrator/hero_poster');
        }
    }
    public function delete_hero($id, $img)
    {

        $file_path = "assets/img/$img";
        if (unlink($file_path)) {
            $this->db->delete('hero_img', array('id' => $id));
            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil menghapus hero</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">
            gagal menghapus hero</div>');
        }
        redirect('administrator/hero_poster');
    }
}
