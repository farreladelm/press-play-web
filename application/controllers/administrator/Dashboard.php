<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user_model', 'user');

        if (isset($this->session->userdata['role_id'])) {
            if ($this->session->userdata['role_id'] == 2) {
                redirect('auth/logout');
                // membuat flashdata message untuk menandakan perlu login
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Kamu perlu login terlebih dahulu
                </div>');
                redirect('auth');
            }
        } else {
            // membuat flashdata message untuk menandakan perlu login
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Kamu perlu login terlebih dahulu
            </div>');
            redirect('auth');
        }
    }

    public function index()
    {
        $data['title'] = 'Dashboard';

        if ($this->input->get()) {
            $data_input = $this->input->get();
            $data['search'] = $data_input['search'];
            if ($this->input->get('kategori') == '') {
                $data['current_category'] = '';
                $data['users'] = $this->user->get_user($data_input['search']);
                // print_r($data);
            } else {
                $data['current_category'] = $data_input['kategori'];
                $data['users'] = $this->user->get_user($data_input['search'], $data_input['kategori']);
                // print_r($data);
            }
        } else {
            $data['current_category'] = '';
            $data['search'] = '';
            $data['users'] = $this->user->get_all_user();
        }
        $this->template_admin->load('admin/user/users', $data);
    }

    public function update_user($id)
    {
        // melakukan validasi form
        $this->form_validation->set_rules('first_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

        // jika validasi gagal
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Update User';
            $data['user'] = $this->user->get_single_user($id);
            $data['id'] = $id;
            // print_r($data);
            $this->template_admin->load('admin/user/update_user', $data);
        }

        // jika validasi berhasil
        else {

            $data = [
                'first_name' => htmlspecialchars($this->input->post('first_name', true)),
                'last_name' => htmlspecialchars($this->input->post('last_name', true)),
                'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'address' => htmlspecialchars($this->input->post('address', true)),
                'role_id' => htmlspecialchars($this->input->post('role_id', true)),
                // 'registered_at' => time(),
            ];
            // menginputkan data registrasi

            $this->db->where('id', $id);
            $this->db->update('user', $data);

            // memberikan flashdata message kalau registrasi berhasil
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Mengubah user</div>');

            // redirect ke controller auth, atau login
            echo $id;
            redirect('administrator/dashboard');
        }
    }

    public function delete_user($id)
    {

        if ($this->db->delete('user', array('id' => $id))) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            Berhasil menghapus User</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">
            gagal menghapus User</div>');
        }
        redirect('administrator/dashboard');
    }
}
