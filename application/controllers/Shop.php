<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shop extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function index()
    {
        if ($this->input->get()) {
            $get_data = $this->input->get();
            $where = 0;
            if ($get_data['harga'] == "100.000 - 499.000") {
                $where = "price > '100000' AND price < '499000'";
            } elseif ($get_data['harga'] == "< 99.000") {
                $where = "price > '0' AND price < '99000'";
            } elseif ($get_data['harga'] == "> 500.000") {
                $where = "price > '500000' AND price < '2000000'";
            } else {
                $where = "price > '0'";
            }
            if ($get_data['kategori'] == null) {
                $this->db->where($where);
                $this->db->where('status', 'APPROVED');
                $this->db->like('nama', $get_data['search']);
                $this->db->join('sale', 'sale.product_id = products.id');
                $this->db->from('products');
            } else {
                $this->db->where($where);
                $this->db->where('status', 'APPROVED');
                $this->db->where('category', $get_data['kategori']);
                $this->db->like('nama', $get_data['search']);
                $this->db->join('sale', 'sale.product_id = products.id');
                $this->db->from('products');
            }
            $data['current_harga'] = $get_data['harga'];
            $data['current_category'] = $get_data['kategori'];
            $data['search'] = $get_data['search'];
            $result = $this->db->get()->result_array();
            $data['products'] = $result;
        } else {
            $data['products'] = $this->product_model->get_approved_product();
            $data['current_category'] = 'Semua Kategori';
            $data['search'] = '';
            $data['current_harga'] = '';

            // print_r($data);
        }
        $data['title'] = "Press Play | Shop";
        $data['categories'] = $this->product_model->get_category();
        $this->template->load('client/shop', $data);
    }

    public function product($id)
    {
        $data_sess = [
            'current_product' => $this->product_model->get_approved_product($id)
        ];
        $this->session->set_userdata($data_sess);
        if ($this->session->userdata['first_name']) {
            $data['product'] = $this->product_model->get_approved_product($id);
            $data['title'] = 'Press Play | Produk';
            $data['id'] = $id;
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            // print_r($data);
            $this->template->load('client/display_single_product', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Kamu perlu login terlebih dahulu
                </div>');
            redirect('auth');
        }
    }

    public function buy_product($id)
    {
        $count = htmlspecialchars($this->input->post('count', true));
        $this->product_model->buy_product($id, $count);
        $this->template->load('client/buy_product');
    }
}
