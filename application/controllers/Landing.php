<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('hero_model');
        $this->load->model('product_model');
    }

    public function index()
    {
        $data['hero_arr'] = $this->hero_model->get_hero();
        $data['products'] = $this->product_model->get_approved_product(null, 4);
        // print_r($data['products']);
        $data['top_products'] = $this->product_model->top_gross_product();
        $data['title'] = "Press Play";
        $this->template->load('landing_page', $data);
    }
}
