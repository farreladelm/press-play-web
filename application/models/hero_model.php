<?php
defined('BASEPATH') or exit('No direct script access allowed');

class hero_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function get_hero($id = null)
    {
        if ($id) {
            $this->db->select('*');
            $this->db->from('hero_img');
            $this->db->where('id', $id);
            $get = $this->db->get();

            return $get->result_array()[0];
        }
        $this->db->select('*');
        $this->db->from('hero_img');
        $this->db->where('status', 'APPROVED');
        $this->db->order_by('priority', 'DESC');
        $this->db->limit(4);
        $get = $this->db->get();

        return $get->result_array();
    }

    public function update_hero($id, $data)
    {
        $this->db->where($id);
        $this->db->update('hero_img', $data);
    }

    public function get_all_hero()
    {
        $this->db->select('*');
        $this->db->from('hero_img');
        $this->db->order_by('priority', 'DESC');
        $get = $this->db->get();

        return $get->result_array();
    }

    public function approve_hero($id)
    {
        $data = ['status' => 'APPROVED'];
        $this->db->where('id', $id);
        $this->db->update('hero_img', $data);

        if ($this->db->affected_rows()) {
            return true;
        }
        return false;
    }
    public function unapprove_hero($id)
    {
        $data = ['status' => 'REJECTED'];
        $this->db->where('id', $id);
        $this->db->update('hero_img', $data);

        if ($this->db->affected_rows()) {
            return true;
        }
        return false;
    }
}
