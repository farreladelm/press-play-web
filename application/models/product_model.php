<?php
defined('BASEPATH') or exit('No direct script access allowed');

class product_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    // =================== ADMINISTRATOR =========================================
    public function get_product($id = null, $limit = null)
    {
        if ($id != null) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('id', $id);

            $get = $this->db->get();

            return $get->result_array()[0];
        }
        if ($limit != null) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->limit($limit);
            $get = $this->db->get();

            return $get->result_array();
        } else {
            $this->db->select('*');
            $this->db->from('products');
            $get = $this->db->get();

            return $get->result_array();
        }
    }
    // =================== CLIENT =========================================
    // =================== ADMINISTRATOR & CLIENT =========================================


    public function get_approved_product($id = null, $limit = null)
    {
        if ($id) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('products.id', $id);
            $this->db->where('status', 'APPROVED');
            $this->db->join('sale', 'sale.product_id = products.id ');

            $get = $this->db->get();

            return $get->result_array()[0];
        }
        if ($limit) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('status', 'APPROVED');
            $this->db->join('sale', 'sale.product_id = products.id ');
            $this->db->limit($limit);
            $get = $this->db->get();

            return $get->result_array();
        }
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $this->db->join('sale', 'sale.product_id = products.id ');
        $get = $this->db->get();

        return $get->result_array();
    }

    public function get_all_product($search = null)
    {
        if ($search) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->like('nama', $search);
            $get = $this->db->get();
        } else {
            $this->db->select('*');
            $this->db->from('products');
            $get = $this->db->get();
        }

        return $get->result_array();
    }


    public function search_product($search, $price, $category)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $this->db->where('price', '');
        $this->db->like('nama', $search);
        $this->db->join('sale', 'sale.product_id = products.id');
        $get = $this->db->get();

        return $get->result_array();
    }
    public function category_product($category)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $this->db->where('category', $category);
        $this->db->join('sale', 'sale.product_id = products.id');
        $get = $this->db->get();

        return $get->result_array();
    }
    public function category_search_product($category, $search)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('category', $category);
        $this->db->where('status', 'APPROVED');
        $this->db->like('nama', $search);
        $this->db->join('sale', 'sale.product_id = products.id');
        $get = $this->db->get();

        return $get->result_array();
    }

    public function top_gross_product()
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $this->db->join('sale', 'sale.product_id = products.id ');
        $this->db->order_by('count', 'DESC');
        $this->db->limit(4);
        $get = $this->db->get();

        return $get->result_array();
    }

    public function get_category()
    {
        $this->db->distinct();
        $this->db->select('category');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $get = $this->db->get();

        return $get->result_array();
    }

    //  ============================================================================================

    public function product_by_status()
    {
        // #1 SubQueries no.1 -------------------------------------------

        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'PENDING');
        $subQuery1 = $this->db->get()->result_array();


        // #2 SubQueries no.2 -------------------------------------------

        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $subQuery2 = $this->db->get()->result_array();

        // #2 SubQueries no.2 -------------------------------------------

        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'REJECTED');
        $subQuery3 = $this->db->get()->result_array();


        // #3 Union with Simple Manual Queries --------------------------

        // $this->db->query("select * from ($subQuery1 UNION $subQuery2) as unionTable");

        // Merge both query results

        $query = array_merge($subQuery1, $subQuery2);
        $query = array_merge($query, $subQuery3);

        return $query;
    }

    // ========================= BUY PRODUCT ======================================

    public function buy_product($id, $v)
    {
        $this->db->select('count');
        $this->db->from('sale');
        $this->db->where('product_id', $id);

        $count = $this->db->get()->result_array()[0];
        $count = $count['count'];
        $count = $count + $v;

        $this->db->select('stock');
        $this->db->from('products');
        $this->db->where('id', $id);

        $stock = $this->db->get()->result_array()[0];
        $stock = $stock['stock'];
        $stock = $stock - $v;

        $this->db->where('product_id', $id);
        $this->db->update('sale', ['count' => $count]);

        $this->db->where('id', $id);
        $this->db->update('products', ['stock' => $stock]);
    }

    // ===================== ONLINE PRODUCT ORDER ====================================

    public function product_by_sale($search = null)
    {
        if ($search) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('status', 'APPROVED');
            $this->db->like('nama', $search);
            $this->db->join('sale', 'sale.product_id = products.id ');
            $this->db->order_by('count', 'DESC');
            $get = $this->db->get();

            return $get->result_array();
        }
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('status', 'APPROVED');
        $this->db->join('sale', 'sale.product_id = products.id ');
        $this->db->order_by('count', 'DESC');
        $get = $this->db->get();

        return $get->result_array();
    }
}
