<?php
defined('BASEPATH') or exit('No direct script access allowed');

class user_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    // =================== ADMINISTRATOR =========================================
    public function get_all_user()
    {
        $this->db->select('*');
        $this->db->from('user');
        $get = $this->db->get();

        return $get->result_array();
    }
    public function get_user($search, $role_id = null)
    {
        if ($role_id != null) {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('role_id', $role_id);
            $this->db->like('first_name', $search);
            $get = $this->db->get();

            return $get->result_array();
        }
        $this->db->select('*');
        $this->db->from('user');
        $this->db->like('first_name', $search);
        $get = $this->db->get();

        return $get->result_array();
    }

    public function get_single_user($id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $id);

        $get = $this->db->get();

        return $get->result_array()[0];
    }
    // public function get_staff()
    // {
    //     return $this->get_user(1);
    // }
    // public function get_client()
    // {
    //     return $this->get_user(2);
    // }
    // public function get_admin()
    // {
    //     return $this->get_user(0);
    // }
}
