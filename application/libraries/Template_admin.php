<?php
defined('BASEPATH') or exit('Not Allowed Direct Access');

class Template_admin
{
    var $ci;

    function __construct()
    {
        $this->ci = &get_instance();
    }

    public function load($body_view = null, $data = [])
    {
        $body = $this->ci->load->view($body_view, $data, true);

        $data['body'] = $body;

        $tpl_view = 'admin/template/layout';

        $this->ci->load->view($tpl_view, $data);
    }
}
