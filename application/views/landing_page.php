<!-- hero slider -->
<div class="slider-container">
    <!-- Slider main container -->
    <div class="swiper">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php foreach ($hero_arr as $key => $rows) : ?>
                <div class="swiper-slide">
                    <img src="<?= base_url('assets/img/') ?><?= $rows['alamat_img'] ?>" alt="<?= $key ?>">
                </div>
            <?php endforeach; ?>
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

    </div>
</div>
<!-- end hero-slider -->

<!-- start best selling product section -->

<section class="py-5 best-product ">
    <h1 class="fw-bold heading">Best Selling Product</h1>
    <hr>
    <p class="mt-3 mb-4 text-center">
        Produk-produk yang selalu menjadi favorit gamers Indonesia!
    </p>
    <div class="product-con row container-fluid justify-content-center">
        <?php foreach ($top_products as $product) :
            $id = $product['product_id'];
        ?>
            <a href="<?= base_url("shop/product/$id") ?>" class="product col-lg-3 col-md-4 col-sm-12">
                <img class="img-fluid" src="<?= base_url('assets/img') ?>/<?= $product['image'] ?>">
                <div class="detail-product mt-2">
                    <h3 class="elipsis fs-6 fw-bold text-uppercase"><?= strlen($product['nama']) > 30 ? substr($product['nama'], 0, 30) . "..." : $product['nama']; ?></h3>
                    <div class="ms-3 m-detail d-flex flex-column justify-content-between">
                        <span class="price">
                            Rp<?= number_format($product['price'], 2, '.', ','); ?>
                        </span>
                        <span class="sale">
                            <?= $product['count'] ?> Terjual
                        </span>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
    <div class="container d-flex justify-content-end mt-3"><a href="<?= site_url('shop') ?>" class="ms-auto d-flex align-items-center gap-2">See more product <i class="fs-6 fa-solid fa-arrow-right-to-bracket"></i></a></div>
</section>

<section class="py-5 best-product ">
    <h1 class="fw-bold heading">New Coming Product</h1>
    <hr>
    <p class="mt-3 mb-4 text-center">
        Produk-produk terbaru dari Press Play!
    </p>
    <div class="product-con row container-fluid justify-content-center">
        <?php foreach ($products as $product) :
            $id = $product['product_id'];
        ?>
            <a href="<?= base_url("shop/product/$id") ?>" class="product col-lg-3 col-md-4 col-sm-12">
                <img class="img-fluid" src="<?= base_url('assets/img') ?>/<?= $product['image'] ?>">
                <div class="detail-product mt-2">
                    <h3 class="elipsis fs-6 fw-bold text-uppercase"><?= strlen($product['nama']) > 30 ? substr($product['nama'], 0, 30) . "..." : $product['nama']; ?></h3>
                    <div class="ms-3 m-detail d-flex flex-column justify-content-between">
                        <span class="price">
                            Rp<?= number_format($product['price'], 2, '.', ','); ?>
                        </span>
                        <span class="sale">
                            <?= $product['count'] ?> Terjual
                        </span>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
    <div class="container d-flex justify-content-end mt-3"><a href="<?= site_url('shop') ?>" class="ms-auto d-flex align-items-center gap-2">See more product <i class="fs-6 fa-solid fa-arrow-right-to-bracket"></i></a></div>
</section>

<section class="p-5 container">
    <div class="row g-3">
        <div class="col-md-4 text-center">
            <img class="image-fluid" src="<?= base_url('assets/img/') ?>promo-1.jpg">
        </div>
        <div class="col-md-8 d-flex align-items-center ">
            <p class="ms-lg-5 ps-lg-5 fs-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet eaque atque consectetur minus accusamus a maiores architecto, ad cumque quas! Magnam dignissimos libero soluta error minus fugiat aut eum qui? Qui excepturi eaque consequuntur cum beatae, aperiam porro, officia minima, perspiciatis aspernatur exercitationem officiis? Molestiae beatae ullam sint doloremque quos.</p>
        </div>
    </div>
</section>