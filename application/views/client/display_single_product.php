<div class="container py-5 mb-5 d-flex justify-content-center">
    <div class="py-5 m-lg-4 m-md-2 mb-5 container row align-items-center justify-content-center border">
        <div class="col-md-4 m-md-5">
            <img class="rounded" src="<?= base_url('assets/img') ?>/<?= $product['image'] ?>" alt="<?= $product['nama'] ?>">
        </div>
        <form class="col-md-6 mt-lg-0 mt-2" action="<?= site_url("shop/buy_product/$id") ?>" method="post">
            <h4 class="mb-3 fw-semibold"><?= $product['nama'] ?></h4>
            <hr class="divider mb-3">
            <h3 class="mb-3 fw-bold">Rp<?= number_format($product['price'], 2, '.', ','); ?></h3>
            <small class="small text-gray d-flex align-items-center mb-3">
                Terjual <span class="ms-1"><?= $product['count'] ?></span>
            </small>
            <p><?= $product['details'] ?></p>
            <small class="small text-gray d-flex align-items-center">Stok <span class="ms-1 p-2 border rounded"><?= $product['stock'] ?></span></small>
            <div class="mt-3">
                <div id="minus" class='btn border rounded py-1 px-2 bg-light text-gray'>-</div>
                <input id="counter" name="count" class="border rounded py-1 px-2 text-center" style="width: 50px;" type="text" value='0' />
                <div id="plus" class='btn border rounded py-1 px-2 bg-light text-gray' onclick="changeCount(1)">+</div>
            </div>
            <button class="btn btn-primary mt-lg-5 mt-2" type="submit">Beli Product</button>
        </form>
    </div>
</div>