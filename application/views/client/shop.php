<div class="container">
    <nav class="navbar navbar-expand-lg border rounded">
        <div class="container">

            <form class="container d-flex flex-wrap" action="<?= site_url('shop') ?>">
                <ul class="navbar-nav me-md-auto mb-2 mb-lg-0 d-flex">
                    <li class="nav-item d-flex align-items-center  ">
                        <select class="form-select m-2" aria-label="Default select example" name="kategori">

                            <option selected value="">Semua Kategori</option>
                            <?php foreach ($categories as $category) : ?>
                                <option value="<?= $category['category'] ?>" <?= $current_category == $category['category'] ? 'selected' : '' ?>><?= $category['category'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>
                    <li class="nav-item d-flex align-items-center  ">
                        <select class="form-select m-2" aria-label="Default select example" name="harga">

                            <option selected value="">Semua Harga</option>
                            <option value="< 99.000" <?= $current_harga == "< 99.000" ? 'selected' : '' ?>>
                                < 99.000 </option>
                            <option value="100.000 - 499.000" <?= $current_harga == "100.000 - 499.000" ? 'selected' : '' ?>>
                                100.000 - 499.000
                            </option>
                            <option value="> 500.000" <?= $current_harga == "> 500.000" ? 'selected' : '' ?>>
                                > 500.000
                            </option>
                        </select>
                    </li>
                    <li class="nav-item d-flex align-items-center  ">
                        <button class="shadow-none bg-accent border-0 p-2 rounded" type="submit">
                            <i class="fs-5 text-light fa-solid fa-filter"></i>
                        </button>
                        <a class="shadow-none bg-accent border-0 p-2 rounded ms-2" href="<?= site_url('shop') ?>">
                            <i class="fs-5 text-light fa-solid fa-filter-circle-xmark"></i>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ">
                    <li class="nav-item d-flex align-items-center">
                        <input class="form-control me-2" value="<?= isset($search) ? $search : '' ?>" name="search" type="search" placeholder="Search" aria-label="Search">
                        <button class="shadow-none bg-accent border-0 p-2 rounded" type="submit"><i class="fs-5 text-light fa-solid fa-magnifying-glass"></i></button>
                    </li>
                </ul>
            </form>


        </div>
    </nav>

</div>
<div class="container d-flex justify-content-center">
    <div class="product-con row justify-content-center">
        <?php if ($products) : ?>
            <?php foreach ($products as $product) :
                $id = $product['product_id'];
            ?>
                <a href="<?= base_url("shop/product/$id") ?>" class="product col-lg-3 col-md-4 col-sm-12" style="width: 250px;">
                    <img class="img-fluid" src="<?= base_url('assets/img') ?>/<?= $product['image'] ?>">
                    <div class="detail-product mt-2">
                        <h3 class="elipsis fs-6 fw-bold text-uppercase"><?= strlen($product['nama']) > 30 ? substr($product['nama'], 0, 20) . "..." : $product['nama']; ?></h3>
                        <div class="ms-3 m-detail d-flex flex-column justify-content-between">
                            <span class="price">
                                Rp<?= number_format($product['price'], 2, '.', ','); ?>
                            </span>
                            <span class="sale">
                                <?= $product['count'] ?> Terjual
                            </span>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        <?php else : ?>
            <p class="text-gray my-5 py-5 h-100">Cek Kembali Pencarian anda</p>
        <?php endif; ?>
    </div>
</div>