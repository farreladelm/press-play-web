<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/css') ?>/bootstrap.min.css">
    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/favicon') ?>/favicon.ico">
    <script src="<?= base_url('assets/vendor/js') ?>/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/1cf4c7dee0.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?= base_url('assets/css/') ?>style.css" />
    <title><?= $title ?></title>

</head>

<body class="bg-accent-gradient">