<!-- <section class="container2">
    <div class="auth-container">
        <div class="login-form">
            <form action="<?= base_url('auth') ?>" method="POST">
                <h3 class="fw-bold">Login</h3>


                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" required>
                </div>
                <span class="f-pass"><a href="#">Lupa password?</a></span>
                <button type="submit" class="btn" name="submit" value="submit">
                    <span>Play<i class="fa-solid fa-play"></i></span>

                </button>
            </form>
        </div>
        <div class="register-con">
            <p class="regis-text">
                <b>Belum memiliki akun?</b> <br>
                Ayo daftarkan dirimu sekarang, dan dapatkan potongan harga sebesar 15%*
            </p>
            <a class="btn btn-regis" href="<?= base_url('auth/registration') ?>">
                <span>Register<i class="fa-solid fa-play"></i></span>

            </a>
        </div>
    </div>
</section> -->

<section class="container d-flex justify-content-center align-items-center" style="min-height: 100vh;">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-xl-10">
                <div class="card rounded-3 text-black border-0 overflow-hidden">
                    <div class="row g-0">
                        <div class="col-lg-6">
                            <div class="card-body p-md-5 mx-md-4">

                                <div class="text-center d-flex align-items-center justify-content-center">
                                    <img class="rounded me-2" src="<?= base_url() ?>assets/img/logo.png" alt="logo" style="width: 50px;">
                                    <span class="text-uppercase fw-bold accent-col fs-4">Press Play</span>
                                </div>

                                <form class="mt-3" action="<?= base_url('auth') ?>" method="POST">
                                    <!-- menampilkan flashdata -->
                                    <?= $this->session->flashdata('message') ?>
                                    <div class="form-outline mb-4">
                                        <label class="form-label fw-semibold" for="email">Email</label>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Masukkan alamat email anda" />
                                    </div>

                                    <div class="form-outline mb-4">
                                        <label class="form-label fw-semibold" for="password">Password</label>
                                        <input type="password" name="password" id="password" class="form-control" />
                                    </div>

                                    <div class="text-center pt-1 mb-5 pb-1">
                                        <button type="submit" class="btn-play mx-auto mb-2" name="submit" value="submit">
                                            <span>Play<i class="fa-solid fa-play"></i></span>
                                        </button>
                                        <a class="text-muted" href="#!">Forgot password?</a>
                                    </div>

                                    <div class="d-flex align-items-center justify-content-center pb-4">
                                        <p class="mb-0 me-2">Don't have an account?</p>
                                        <a class="accent-col fw-semibold" href="<?= base_url('auth/registration') ?>">
                                            Register
                                        </a>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="col-lg-6 d-flex align-items-center bg-accent">
                            <div class=" text-white px-3 py-4 p-md-5 mx-md-4">
                                <h4 class="mb-4 fw-semibold">We are more than just a company</h4>
                                <p class="small mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>