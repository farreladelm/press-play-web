<!-- <section class="container">
    <div class="auth-container">
        <div class="login-form">
            <form action="<?= base_url('auth/registration') ?>" method="POST">
                <h3 class="text-uppercase fw-bold">Registration</h3>
                <div class="form-group">
                    <label for="first_name">First name</label>
                    <input type="text" class="form-control form-control-user" id="first_name" name="first_name" value="<?= set_value('first_name') ?>">

                    <?= form_error('first_name', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="last_name">Last name</label>
                    <input type="text" class="form-control form-control-user" id="last_name" name="last_name" value="<?= set_value('last_name') ?>">

                    <?= form_error('last_name', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control form-control-user" id="email" name="email" value="<?= set_value('email') ?>">

                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="no_hp">Nomor HP</label>
                    <input type="text" class="form-control form-control-user" id="no_hp" name="no_hp" value="<?= set_value('no_hp') ?>">

                    <?= form_error('no_hp', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control form-control-user" id="address" name="address" value="<?= set_value('address') ?>">

                    <?= form_error('address', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="password1">Password</label>
                    <input type="password" class="form-control form-control-user" id="password1" name="password1" value="<?= set_value('password1') ?>">

                    <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group">
                    <label for="password2">Re-enter Password</label>
                    <input type="password" class="form-control form-control-user" id="password2" name="password2" value="<?= set_value('password2') ?>">

                    <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
                </div>
                <button type="submit" class="btn" name="submit" value="submit">
                    <span>Register<i class="fa-solid fa-play"></i></span>
                </button>
            </form>
        </div>
    </div>
</section> -->

<section class="container d-flex justify-content-center align-items-center" style="min-height: 100vh;">
    <div class="row">
        <div class="mx-auto col-md-6">
            <form class="row g-3 p-lg-5 p-3 border rounded bg-light" action="<?= base_url('auth/registration') ?>" method="POST">
                <div class="col-12 text-center mb-3">
                    <div class="text-center d-flex align-items-center justify-content-center">
                        <img class="rounded me-2" src="<?= base_url() ?>assets/img/logo.png" alt="logo" style="width: 50px;">
                        <span class="text-uppercase fw-bold accent-col fs-4">Press Play</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="first_name" class="form-label">Nama Depan</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" value="<?= set_value('first_name') ?>">
                    <?= form_error('first_name', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-md-6">
                    <label for="last_name" class="form-label">Nama Belakang</label>
                    <input type="text" name="last_name" class="form-control" id="last_name" value="<?= set_value('last_name') ?>">
                    <?= form_error('last_name', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-md-6">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" id="email" value="<?= set_value('email') ?>">
                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-md-6">
                    <label for="no_hp" class="form-label">Nomor HP</label>
                    <input type="text" name="no_hp" class="form-control" id="no_hp" value="<?= set_value('no_hp') ?>">
                    <?= form_error('no_hp', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-12">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" name="address" class="form-control" id="address" value="<?= set_value('address') ?>">
                    <?= form_error('address', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-md-6">
                    <label for="password1" class="form-label">Password</label>
                    <input type="password" name="password1" class="form-control" id="password1" value="<?= set_value('password1') ?>">
                    <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-md-6">
                    <label for="password2" class="form-label">Re-Enter Password</label>
                    <input type="password" name="password2" class="form-control" id="password2">
                    <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary bg-accent mx-auto my-3">Register</button>
                </div>
                <div class="d-flex align-items-center justify-content-center pb-4">
                    <p class="mb-0 me-2">Sudah memiliki akun?</p>
                    <a class="accent-col fw-semibold" href="<?= base_url('auth') ?>">
                        Login
                    </a>
                </div>
            </form>

        </div>
</section>