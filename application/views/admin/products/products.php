<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800 fw-bold">Daftar Produk</h1>

<?php if (isset($_SESSION['role_id'])) : ?>
    <?php if ($_SESSION['role_id'] == 1) : ?>
        <a class="btn btn-success mb-3" href="<?= site_url('administrator/products/add_product') ?>">Tambah +</a>
    <?php endif; ?>
<?php endif; ?>
<nav class="navbar navbar-expand-lg border rounded shadow" style="background-color: #fff;">

    <form class="d-flex flex-wrap" action="<?= site_url('administrator/products') ?>" style="width: 100%;">
        <ul class="navbar-nav me-md-auto mb-2 mb-lg-0 d-flex">
            <li class="nav-item d-flex align-items-center  ">
                <select class="form-select m-2" aria-label="Default select example" name="kategori">

                    <option selected value="">Semua Kategori</option>
                    <?php foreach ($categories as $category) : ?>
                        <option value="<?= $category['category'] ?>" <?= $current_category == $category['category'] ? 'selected' : '' ?>><?= $category['category'] ?></option>
                    <?php endforeach; ?>
                </select>
            </li>
            <li class="nav-item d-flex align-items-center  ">
                <button class="shadow-none bg-primary border-0 p-2 rounded" type="submit">
                    <i class="fs-5 text-light fa-solid fa-filter"></i>
                </button>
                <a class="shadow-none bg-primary border-0 p-2 rounded ms-2" href="<?= site_url('administrator/products') ?>">
                    <i class="fs-5 text-light fa-solid fa-filter-circle-xmark"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ">
            <li class="nav-item d-flex align-items-center">
                <input class="form-control me-2" value="<?= isset($search) ? $search : '' ?>" name="search" type="search" placeholder="Search" aria-label="Search">
                <button class="shadow-none bg-primary border-0 p-2 rounded" type="submit"><i class="fs-5 text-light fa-solid fa-magnifying-glass"></i></button>
            </li>
        </ul>
    </form>


</nav>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <?= $this->session->flashdata('message') ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Nama Produk</th>
                        <th>detail Produk</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Kategori</th>
                        <th>Status</th>
                        <th>Dibuat Oleh</th>
                        <th>Dibuat Pada</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($products) : ?>
                        <?php foreach ($products as $key => $product) :
                            $id = $product['id'];
                        ?>
                            <tr>
                                <td><?= ++$key ?></td>
                                <td class="text-center"><img src="<?= base_url('assets/img/') ?><?= $product['image'] ?>" width="100" height="auto"> </td>
                                <td><?= $product['nama'] ?></td>
                                <td><?= strlen($product['details']) > 30 ? substr($product['details'], 0, 30) . "..." : $product['details']; ?></td>
                                <td><?= $product['price'] ?></td>
                                <td><?= $product['stock'] ?></td>
                                <td><?= $product['category'] ?></td>
                                <td><?= $product['status'] ?></td>
                                <td><?= $product['created_by'] ?></td>
                                <td><?= $product['created_date'] ?></td>
                                <td>
                                    <?php if (isset($_SESSION['role_id'])) : ?>

                                        <?php if ($_SESSION['role_id'] == 0) : ?>
                                            <?php if ($product['status'] != 'APPROVED') : ?>

                                                <a href="<?= site_url("administrator/products/approve_product/$id") ?>" class="btn btn-success m-2">Approve</a>
                                            <?php else : ?>
                                                <a href="<?= site_url("administrator/products/unapprove_product/$id") ?>" class="btn btn-danger m-2">Unapprove</a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php if ($_SESSION['role_id'] == 1) : ?>
                                            <a href="<?= site_url("administrator/products/update_product/$id") ?>" class="btn btn-warning m-2">Edit</a>
                                        <?php endif; ?>
                                        <button type="button" class="btn btn-danger m-2" data-toggle="modal" data-target="#delete-modal" data-id="<?= $id ?>" data-nama="<?= $product['nama'] ?>" data-img="<?= $product['image'] ?>">Delete</button>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p class="m-5 p-5 text-center container fs-1">Produk dengan kata kunci '<?= $search ?>' tidak ditemukan</p>
                    <?php endif; ?>

                </tbody>
            </table>

            <div class="modal fade" id="delete-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah anda yakin akan menghapus <span id="delete-nama"></span>?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a id="confirm-delete" type="button" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>