<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800 fw-bold">Daftar Produk Online</h1>

<nav class="navbar navbar-expand-lg border rounded shadow" style="background-color: #fff;">

    <form class="d-flex flex-wrap" action="<?= site_url('administrator/products/online_product') ?>" style="width: 100%;">
        <ul class="navbar-nav me-md-auto mb-2 mb-lg-0 d-flex">
            <li class="nav-item d-flex align-items-center  ">
                <select class="form-select m-2" aria-label="Default select example" name="kategori">

                    <option selected value="">Semua Kategori</option>
                    <?php foreach ($categories as $category) : ?>
                        <option value="<?= $category['category'] ?>" <?= $current_category == $category['category'] ? 'selected' : '' ?>><?= $category['category'] ?></option>
                    <?php endforeach; ?>
                </select>
            </li>
            <li class="nav-item d-flex align-items-center  ">
                <button class="shadow-none bg-primary border-0 p-2 rounded" type="submit">
                    <i class="fs-5 text-light fa-solid fa-filter"></i>
                </button>
                <a class="shadow-none bg-primary border-0 p-2 rounded ms-2" href="<?= site_url('administrator/products/online_product') ?>">
                    <i class="fs-5 text-light fa-solid fa-filter-circle-xmark"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ">
            <li class="nav-item d-flex align-items-center">
                <input class="form-control me-2" value="<?= isset($search) ? $search : '' ?>" name="search" type="search" placeholder="Search" aria-label="Search">
                <button class="shadow-none bg-primary border-0 p-2 rounded" type="submit"><i class="fs-5 text-light fa-solid fa-magnifying-glass"></i></button>
            </li>
        </ul>
    </form>


</nav>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <?= $this->session->flashdata('message') ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Nama Produk</th>
                        <th>detail Produk</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Kategori</th>
                        <th>Jumlah Terjual</th>
                        <th>Total Penjualan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($products) : ?>
                        <?php foreach ($products as $key => $product) :
                            $id = $product['product_id'];
                        ?>
                            <tr>
                                <td><?= ++$key ?></td>
                                <td class="text-center"><img src="<?= base_url('assets/img/') ?><?= $product['image'] ?>" width="100" height="auto"> </td>
                                <td><?= $product['nama'] ?></td>
                                <td><?= strlen($product['details']) > 30 ? substr($product['details'], 0, 30) . "..." : $product['details']; ?></td>
                                <td><?= $product['price'] ?></td>
                                <td><?= $product['stock'] ?></td>
                                <td><?= $product['category'] ?></td>
                                <td><?= $product['count'] ?></td>
                                <td><?= $product['count'] * $product['price']  ?></td>
                                <td>
                                    <?php if (isset($_SESSION['role_id'])) : ?>

                                        <?php if ($_SESSION['role_id'] == 0) : ?>
                                            <a href="<?= site_url("administrator/products/unapprove_product/$id") ?>" class="btn btn-danger m-2">Unapprove</a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p class="m-5 p-5">Produk yang anda cari tidak ada</p>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>