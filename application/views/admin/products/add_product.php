<h1 class="h3 mb-2 text-gray-800">Tambah Produk</h1>

<form class="row g-3" action="<?= site_url('administrator/products/add_product') ?>" method="post" enctype="multipart/form-data">
    <div class="col-md-12 mb-3">
        <label for="nama" class="form-label">Nama Produk</label>
        <input type="text" name="nama" class="form-control" id="nama" placeholder="minimal 5 karakter">
    </div>
    <div class="col-md-3 mb-3">
        <label for="harga" class="form-label">Harga</label>
        <input type="number" name="harga" class="form-control" id="harga">
    </div>
    <div class="col-md-3 mb-3">
        <label for="stok" class="form-label">stok</label>
        <input type="number" name="stok" class="form-control" id="stok" placeholder="Minimal 1">
    </div>
    <div class="col-md-3 mb-3">
        <label for="kategori" class="form-label">Kategori</label>
        <input type="text" name="kategori" class="form-control" id="kategori">
    </div>
    <div class="col-md-12 mb-3">
        <div class="form-floating">
            <label for="floatingTextarea">Detail Produk</label>
            <textarea class="form-control" placeholder="tuliskan detail produk" id="floatingTextarea" name="detail"></textarea>
        </div>
    </div>
    <div class="col-md-3 mb-3">
        <label for="image" class="form-label">Upload gambar</label>
        <input type="file" name="image" id="file" class="form-control">

    </div>
    <div class="col-12">

        <button type="submit" class="btn btn-primary mt-3">Tambah Produk</button>
    </div>
</form>