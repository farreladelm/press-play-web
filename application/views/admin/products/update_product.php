<h1 class="h3 mb-2 text-gray-800">Update Produk</h1>

<form class="row g-3 mt-3" action="<?= site_url("administrator/products/update_product/$id") ?>" method="POST">
    <div class="col-12">
        <label for="nama" class="form-label">Nama Produk</label>
        <input type="text" name="nama" class="form-control" id="nama" placeholder="minimal 5 karakter" value="<?= $produk['nama'] ?>">
        <?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-12">
        <label for="detail" class="form-label">Detail Produk</label>
        <input type="text" name="detail" class="form-control" id="detail" placeholder="minimal 5 karakter" value="<?= $produk['details'] ?>">
        <?= form_error('detail', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="harga" class="form-label">Harga</label>
        <input type="number" name="harga" class="form-control" id="harga" placeholder="Isikan harga produk" value="<?= $produk['price'] ?>">
        <?= form_error('stok', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="stok" class="form-label">stok</label>
        <input type="number" name="stok" class="form-control" id="stok" placeholder="Isikan stok produk" value="<?= $produk['stock'] ?>">
        <?= form_error('stok', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="kategori" class="form-label">Kategori</label>
        <input type="text" name="kategori" class="form-control" id="kategori" placeholder="Isikan Kategori Produk" value="<?= $produk['category'] ?>">
        <?= form_error('kategori', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-primary mt-3">Update Produk</button>
    </div>
</form>