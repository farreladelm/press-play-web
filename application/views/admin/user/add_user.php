<h1 class="h3 mb-2 text-gray-800">Update User</h1>

<form class="row g-3" action="<?= site_url('administrator/products/add_product') ?>" method="post" enctype="multipart/form-data">
    <div class="col-md-6">
        <label for="first_name" class="form-label">Nama Depan</label>
        <input type="text" name="first_name" class="form-control" id="first_name" value="<?= set_value('first_name') ?>">
        <?= form_error('first_name', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="last_name" class="form-label">Nama Belakang</label>
        <input type="text" name="last_name" class="form-control" id="last_name" value="<?= set_value('last_name') ?>">
        <?= form_error('last_name', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="email" class="form-label">Email</label>
        <input type="email" name="email" class="form-control" id="email" value="<?= set_value('email') ?>">
        <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="no_hp" class="form-label">Nomor HP</label>
        <input type="text" name="no_hp" class="form-control" id="no_hp" value="<?= set_value('no_hp') ?>">
        <?= form_error('no_hp', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-12">
        <label for="address" class="form-label">Address</label>
        <input type="text" name="address" class="form-control" id="address" value="<?= set_value('address') ?>">
        <?= form_error('address', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-primary mt-3">Tambah User</button>
    </div>
</form>