<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800 fw-bold">Daftar User</h1>
<nav class="navbar navbar-expand-lg border rounded py-3 mb-2 shadow" style="background-color: #fff;">
    <form class="d-flex justify-content-center" action="<?= site_url('administrator/dashboard?kategori=') ?>" style="width: 100%;">
        <ul class="navbar-nav me-md-auto mb-2 mb-lg-0">
            <li class="nav-item d-flex align-items-center  ">
                <select class="form-select" aria-label="Default select example" name="kategori" onchange="this.form.submit()">

                    <option selected value="">Semua Kategori</option>
                    <option value="0" <?= $current_category == 0 ? 'selected' : '' ?>>Admin</option>
                    <option value="1" <?= $current_category == 1 ? 'selected' : '' ?>>Staff</option>
                    <option value="2" <?= $current_category == 2 ? 'selected' : '' ?>>User</option>
                </select>

            </li>
        </ul>
        <ul class="navbar-nav ">
            <li class="nav-item d-flex">
                <input class="form-control me-2" value="<?= isset($search) ? $search : '' ?>" name="search" type="search" placeholder="Search" aria-label="Search">
                <button class="shadow-none bg-primary border-0 p-2 rounded" type="submit"><i class="fs-5 text-light fa-solid fa-magnifying-glass"></i></button>
            </li>
        </ul>
    </form>


</nav>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <?= $this->session->flashdata('message') ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Nomor HP</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Terdaftar sejak</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($users) : ?>
                        <?php foreach ($users as $key => $user) :
                            $id = $user['id'];
                        ?>
                            <tr>
                                <td><?= ++$key ?></td>
                                <td><?= $user['first_name'] . ' ' . $user['last_name'] ?></td>
                                <td><?= $user['no_hp'] ?></td>
                                <td><?= $user['email'] ?></td>
                                <td><?= $user['address'] ?></td>
                                <?php if ($user['role_id'] == 0) : ?>
                                    <td>Admin</td>
                                <?php endif; ?>
                                <?php if ($user['role_id'] == 1) : ?>
                                    <td>Staff</td>
                                <?php endif; ?>
                                <?php if ($user['role_id'] == 2) : ?>
                                    <td>Client</td>
                                <?php endif; ?>
                                <td><?= $user['registered_at'] ?></td>
                                <td>
                                    <?php if (isset($_SESSION['role_id']) && $user['role_id'] != 0) : ?>

                                        <?php if ($_SESSION['role_id'] == 1) : ?>
                                            <a href="<?= site_url("administrator/dashboard/update_user/$id") ?>" class="btn btn-warning m-2">Edit</a>
                                        <?php endif; ?>
                                        <?php if ($_SESSION['role_id'] == 0 && $user['role_id'] != $_SESSION['role_id']) : ?>
                                            <button type="button" class="btn btn-danger m-2" data-toggle="modal" data-target="#delete-user-modal" data-id="<?= $id ?>" data-nama="<?= $user['first_name'] ?>">Delete</button>
                                        <?php endif; ?>
                                        <?php if ($_SESSION['role_id'] == 1 && $user['role_id'] != $_SESSION['role_id']) : ?>
                                            <button type="button" class="btn btn-danger m-2" data-toggle="modal" data-target="#delete-user-modal" data-id="<?= $id ?>" data-nama="<?= $user['first_name'] ?>">Delete</button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <p class=" m-5 p-5 fs-4 text-center">User Tidak Ditemukan</p>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>

            <div class="modal fade" id="delete-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah anda yakin akan menghapus <span id="delete-nama"></span>?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a id="confirm-delete" type="button" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>