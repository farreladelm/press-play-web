<h1 class="h3 mb-2 text-gray-800">Update Produk</h1>

<form class="row g-3 mt-3" action="<?= site_url("administrator/dashboard/update_user/$id") ?>" method="POST">
    <div class="col-12">
        <label for="first_name" class="form-label">Nama Depan</label>
        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="minimal 5 karakter" value="<?= $user['first_name'] ?>">
        <?= form_error('first_name', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-12">
        <label for="last_name" class="form-label">Nama Belakang</label>
        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="minimal 5 karakter" value="<?= $user['last_name'] ?>">
        <?= form_error('last_name', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-12">
        <label for="no_hp" class="form-label">Nomor Hp</label>
        <input type="text" name="no_hp" class="form-control" id="no_hp" placeholder="minimal 5 karakter" value="<?= $user['no_hp'] ?>">
        <?= form_error('no_hp', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="email" class="form-label">Email</label>
        <input type="text" name="email" class="form-control" id="email" placeholder="Isikan email produk" value="<?= $user['email'] ?>">
        <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <label for="address" class="form-label">Alamat</label>
        <input type="text" name="address" class="form-control" id="address" placeholder="Isikan address produk" value="<?= $user['address'] ?>">
        <?= form_error('address', '<small class="text-danger">', '</small>'); ?>
    </div>
    <div class="col-md-6">
        <select class="form-select" aria-label="Default select example" name="role_id">
            <option value="0" <?= $user['role_id'] == 0 ? 'selected' : '' ?>>Admin</option>
            <option value="2" <?= $user['role_id'] == 2 ? 'selected' : '' ?>>User</option>
        </select>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-primary mt-3">Update User</button>
    </div>
</form>