<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800 fw-bold">Hero images</h1>

<?php if (isset($_SESSION['role_id'])) : ?>
    <?php if ($_SESSION['role_id'] == 1) : ?>
        <a class="btn btn-success mb-3" href="<?= site_url('administrator/hero_poster/add_hero') ?>">Tambah Hero</a>
    <?php endif; ?>
<?php endif; ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <?= $this->session->flashdata('message') ?>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Image</th>
                        <th>Label</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Image</th>
                        <th>Label</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>

                    <?php foreach ($heros as $hero) :
                        $id = $hero['id'];
                    ?>
                        <tr>
                            <td><?= $hero['id'] ?></td>
                            <td class="text-center"><img src="<?= base_url('assets/img/') ?><?= $hero['alamat_img'] ?>" width="100" height="auto"> </td>
                            <td><?= $hero['label'] ?></td>
                            <td><?= $hero['status'] ?></td>
                            <td><?= $hero['priority'] ?></td>
                            <td class='text-center'>
                                <?php if (isset($_SESSION['role_id'])) : ?>
                                    <?php if ($_SESSION['role_id'] == 1) : ?>
                                        <a href="<?= site_url("administrator/hero_poster/update_hero/$id") ?>" class="btn btn-warning">Update</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#del-modal-poster" data-id="<?= $id ?>" data-nama="<?= $hero['alamat_img'] ?>">Delete</button>
                                    <?php endif; ?>
                                    <?php if ($_SESSION['role_id'] == 0) : ?>
                                        <?php if ($hero['status'] == 'APPROVED') : ?>
                                            <a href="<?= site_url("administrator/hero_poster/unapprove_hero/$id") ?>" class="btn btn-danger">Unapprove</a>
                                        <?php else : ?>
                                            <a href="<?= site_url("administrator/hero_poster/approve_hero/$id") ?>" class="btn btn-warning">Approve</a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div class="modal fade" id="del-modal-poster" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah anda yakin akan menghapus <span id="delete-nama"></span>?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a id="confirm-delete" type="button" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>