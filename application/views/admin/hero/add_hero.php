<h1 class="h3 mb-2 text-gray-800">Tambah Poster</h1>

<?php echo form_open_multipart('administrator/hero_poster/add_hero'); ?>
<div class="col-6">
    <label for="label" class="form-label">Label Foto</label>
    <input type="text" name="label" class="form-control" id="label" placeholder="minimal 5 karakter">
</div>
<div class="col-6">
    <label for="priority" class="form-label">Prioritas Gambar</label>
    <input type="number" name="priority" class="form-control" id="priority" placeholder="Semakin tinggi, semakin utama">
</div>
<div class="col-md-3">
    <label for="image" class="form-label mt-2">Upload gambar</label>
    <input type="file" name="image" id="file" class="form-control mb-2">
    <?= $this->session->flashdata('message') ?>
</div>
<div class="col-12">

    <button type="submit" class="btn btn-primary mt-3">Tambah Poster</button>
</div>
</form>