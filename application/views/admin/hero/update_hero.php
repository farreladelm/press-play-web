<h1 class="h3 mb-2 text-gray-800">Update Poster</h1>

<form action="<?= site_url("administrator/hero_poster/update_hero/$id") ?>" method="POST">
    <div class="col-6">
        <label for="label" class="form-label">Label Foto</label>
        <input type="text" name="label" class="form-control" id="label" placeholder="minimal 5 karakter" value="<?= $hero['label'] ?>">
    </div>
    <div class="col-6 mt-2">
        <label for="priority" class="form-label">Prioritas Gambar</label>
        <input type="number" name="priority" class="form-control" id="priority" placeholder="Semakin besar angka, semakin tinggi prioritas" value="<?= $hero['priority'] ?>">
    </div>
    <div class="col-12">

        <button type="submit" class="btn btn-primary mt-3">Update Poster</button>
    </div>
</form>