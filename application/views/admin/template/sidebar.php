        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('admin') ?>">
                <div class="sidebar-brand-icon rounded-circle d-flex justify-content-center">
                    <img class="rounded-circle" src="<?= base_url('assets/img/') ?>logo.png" width="50" height="auto">
                </div>
                <div class=" sidebar-brand-text mx-3">Press Play admin
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?= str_contains(current_url(), base_url('administrator/dashboard')) ? 'active' : '' ?>">
                <a class="nav-link" href="<?= base_url('administrator/dashboard') ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Product
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item <?= str_contains(current_url(), base_url('administrator/products')) && !str_contains(current_url(), 'products/online_product') ? 'active' : '' ?>">
                <a class="nav-link" href="<?= base_url('administrator/products') ?>">
                    <i class="fas fa-solid fa-cart-shopping"></i>
                    <span>Product</span>
                </a>
            </li>
            <li class="nav-item <?= str_contains(current_url(), 'online_product') ? 'active' : '' ?>">
                <a class="nav-link" href="<?= base_url('administrator/products/online_product') ?>">
                    <i class="fas fa-solid fa-cart-shopping"></i>
                    <span>Online Product</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Website
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item <?= str_contains(current_url(), base_url('administrator/hero_poster')) ? 'active' : '' ?>">
                <a class="nav-link" href="<?= base_url('administrator/hero_poster') ?>">
                    <i class="fas fa-solid fa-image"></i>
                    <span>Hero Poster</span>
                </a>
            </li>

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>


        </ul>
        <!-- End of Sidebar -->


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">