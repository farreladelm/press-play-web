<?php $this->load->view('admin/template/header'); ?>
<?php $this->load->view('admin/template/sidebar'); ?>
<?php $this->load->view('admin/template/top_nav'); ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?= $body ?>
    <?php $this->load->view('admin/template/footer'); ?>