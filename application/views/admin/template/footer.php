</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2021</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Delete User Modal -->
<div class="modal fade" id="delete-user-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin akan menghapus <span id="delete-nama"></span>?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="confirm-user-delete" type="button" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ingin Logout?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Apakah kamu ingin mengakhiri sesi?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                <a class="btn btn-primary" href="<?= site_url('auth/logout') ?>">Ya</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/') ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/') ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/') ?>js/sb-admin-2.min.js"></script>
<script>
    $("#file").change(function(e) {

        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

            var file = e.originalEvent.srcElement.files[i];

            var img = document.createElement("img");
            var reader = new FileReader();
            reader.onloadend = function() {
                img.src = reader.result;
                img.width = '200';
                img.class = "rounded";
            }
            reader.readAsDataURL(file);
            $("#file").after(img);
        }
    });
    $(document).ready(function() {
        $('#delete-modal').on('show.bs.modal', function(event) {
            var btn = $(event.relatedTarget);
            console.log(btn);
            nama = btn.data('nama');
            id = btn.data('id');
            img = btn.data('img');

            console.log(nama);
            console.log(id);

            $('#delete-modal').find('#delete-nama').text(nama);
            $('#confirm-delete').attr('href', `<?= site_url('administrator/products/delete_product/') ?>${id}/${img}`);
        })

        $('#del-modal-poster').on('show.bs.modal', function(event) {
            var btn = $(event.relatedTarget);
            console.log(btn);
            nama = btn.data('nama');
            id = btn.data('id');

            $('#del-modal-poster').find('#delete-nama').text(nama);
            $('#confirm-delete').attr('href', `<?= site_url('administrator/hero_poster/delete_hero/') ?>${id}/${nama}`);
        })

        $('#delete-user-modal').on('show.bs.modal', function(event) {
            var btn = $(event.relatedTarget);
            console.log(btn);
            nama = btn.data('nama');
            id = btn.data('id');

            $('#del-modal-poster').find('#delete-nama').text(nama);
            $('#confirm-user-delete').attr('href', `<?= site_url('administrator/dashboard/delete_user/') ?>${id}`);
        })


    })
</script>
<!-- Page level plugins -->
<script src="<?= base_url('assets/') ?>vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?= base_url('assets/') ?>js/demo/chart-area-demo.js"></script>
<script src="<?= base_url('assets/') ?>js/demo/chart-pie-demo.js"></script>

</body>

</html>