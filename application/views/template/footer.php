<footer class="footer pt-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <a href="#" class="logo text-center mx-auto d-block fs-3 mb-3 fw-bold text-light text-uppercase">press play</a>
                <p class="menu">
                    <a href="#">Home</a>
                    <a href="#">Shop</a>
                    <a href="#">About</a>
                    <a href="#">Blog</a>
                    <a href="#">Contact</a>
                </p>
                <div class="soc-med container-fluid d-flex justify-content-center gap-2 ">
                    <a href="#" class="socmed-icon d-flex justify-content-center align-items-center m-3 text-center rounded-3 border border-light border-2"><i class="fa-brands fa-facebook-f text-light"></i></a>
                    <a href="#" class="socmed-icon d-flex justify-content-center align-items-center m-3 text-center rounded-3 border border-light border-2"><i class="fa-brands fa-instagram text-light"></i></a>
                    <a href="#" class="socmed-icon d-flex justify-content-center align-items-center m-3 text-center rounded-3 border border-light border-2"><i class="fa-brands fa-twitter text-light"></i></a>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 text-center">
                <p class="copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This web is a learning project only
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
                <div class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </div>
            </div>
            <div class="modal-body">Apakah kamu ingin logout?</div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="button" data-dismiss="modal">Tidak</button>
                <a class="btn btn-danger" href="<?= site_url('auth/logout') ?>">Ya</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?= base_url('assets/') ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>
    const swiper = new Swiper('.swiper', {
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        // Optional parameters
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });


    function changeCount(delta) {
        return function() {
            var counter = document.getElementById('counter').value;
            var parsed = parseInt(counter);
            var result = parsed + delta;

            if (result < 0) {
                document.getElementById('counter').value = 0;
                return;
            }
            document.getElementById('counter').value = result;
            return;
        }
    }

    document.getElementById('minus').onclick = changeCount(-1);
    document.getElementById('plus').onclick = changeCount(1);
</script>



</body>

</html>