<nav class="navbar navbar-expand-lg px-lg-5 px-sm-2 sticky-top" style="background-color: #fff;">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?= base_url() ?>">
            <img src="<?= base_url() ?>assets/img/logo.png" alt="logo" class="logo__img">
            <span class="text-uppercase fw-bold accent-col ms-2">Press Play</span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link px-lg-3 px-sm-1 text-uppercase" aria-current="page" href="<?= site_url('') ?>">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link px-lg-3 px-sm-1 text-uppercase" aria-current="page" href="<?= site_url('shop') ?>">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link px-lg-3 px-sm-1 text-uppercase disabled" aria-current="page" href="#">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link px-lg-3 px-sm-1 text-uppercase disabled" aria-current="page" href="#">About Us</a>
                </li>
            </ul>

            <ul class="navbar-nav mb-2 mb-lg-0">
                <?php if (isset($this->session->userdata['first_name'])) : ?>
                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-3 d-none d-lg-inline text-gray-600 small me-2"><?= isset($this->session->userdata['first_name']) ? $this->session->userdata['first_name'] : '' ?></span>
                            <i class="fa-regular fa-user"></i>
                        </a>
                        <!-- Dropdown - User Information -->
                        <ul class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <li>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray"></i>
                                    Profile
                                </a>
                            </li>
                            <li>
                                <div class="dropdown-divider"></div>
                            </li>
                            <li>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php else : ?>
                    <li class="nav-item">
                        <a class="nav-icon nav-link px-lg-3 px-sm-1 text-uppercase" aria-current="page" href="<?= base_url('auth') ?>">
                            <i class="fa-regular fa-user"></i>
                        </a>
                    </li>
                <?php endif; ?>
                <li class="nav-item">
                    <a class="nav-icon nav-link px-lg-3 px-sm-1 text-uppercase" aria-current="page" href="#"><i class="fa-solid fa-bag-shopping"></i></a>
                </li>


            </ul>

        </div>
    </div>

</nav>